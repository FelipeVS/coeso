require("tns-core-modules/ui/activity-indicator");
require("tns-core-modules/ui/action-bar");

let vmModule = require("./main-view-model");
let observableModule = require("data/observable");
let mapsModule = require("nativescript-google-maps-sdk");
let Image = require("ui/image").Image;
let imageSource = require("image-source");
let Color = require("color").Color;
let style = require('./map-style.json');
let http = require("http");
let appSettings = require("application-settings");

let page = null;
let mapServiceUrl = 'http://mapa.coesa.com/coesa.com/RESTAPI/';
let coesoWebUrl = "https://coeso-web.herokuapp.com/"
let serverToken = appSettings.getString("serverToken", null)[0] !== '<' ? appSettings.getString("serverToken", null) : undefined;
let busList = [
    '110',
    '2110',
    '4110',
    '423',
    '426',
    '428',
]
let routeList = buildRouteList(busList);
let mapUpdateTime = 5000;

console.log("Cached token: ", serverToken);

function buildRouteList(stringList) {
    let outputList = [];
    stringList.map(line => {
        let icon = new Image();
        icon.imageSource = imageSource.fromResource('i' + line);
        let obj = {
            name: line,
            icon: icon,
            markers: []
        }
        outputList.push(obj);
    })
    return outputList
}

function wait(milliSeconds) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(milliSeconds);
        }, milliSeconds);
    });
}

function pageLoaded(args) {
    page = args.object;
    page.bindingContext = vmModule.mainViewModel;

    getToken(coesoWebUrl);
}

function getToken(url) {
    console.log("Sending signal to mothership...");
    http.getJSON(url)
        .then(response => {
            serverToken = response.token;
            console.log(response.token);
            appSettings.setString("serverToken", serverToken);
            page.bindingContext.isLoading = false;
        })
        .catch(error => {
            console.log("COESO-WEB ERROR:", error);
            serverToken = serverToken ? serverToken : undefined;
            page.bindingContext.isLoading = false;
        })
}

function createMarker(obj, icon) {
    let location = JSON.parse(obj.Localization)
    let direction = obj.Direction === "ASC" ? "Ida" : "Volta"
    let marker = new mapsModule.Marker();

    marker.position = mapsModule.Position.positionFromLatLng(location.coordinates[1], location.coordinates[0]);
    marker.title = `${obj.RouteNumber} (${direction})`;
    marker.snippet = obj.VehiclePlate;
    marker.userData = {
        index: 1
    };
    marker.icon = icon;
    return marker;
}

function isRoutesLoading(bool) {
    page.bindingContext.isRoutesLoading = bool;
}

function onMapReady(args) {
    let mapView = args.object;
    mapView.setStyle(style);
    setInterval(() => {

        if (serverToken) {
            isRoutesLoading(true);
            let promises = [];
            routeList.map(route => {
                let promise = new Promise((resolve, reject) => {
                    return http.getJSON(mapServiceUrl + serverToken + '/MobileActualState/Get?route=' + route.name).then(
                        result => resolve(result),
                        error => reject(error)
                    )
                });
                promises.push(promise)
            })
            Promise.all(promises)
            .then(
                results => {
                    isRoutesLoading(false);
                    mapView.removeAllMarkers()
                    results.map((result, index) => {
                        result.map(bus => {
                            let marker = createMarker(bus, routeList[index].icon)
                            mapView.addMarker(marker);
                        })
                    })
                },
                error => {
                    console.log(error);
                }
            )
        } else {
            console.log("Waiting token...");
        }
    }, mapUpdateTime)
}

function onCoordinateTapped(args) {
    console.log("Coordinate Tapped, Lat: " + args.position.latitude + ", Lon: " + args.position.longitude, args);
}

function onMarkerEvent(args) {
    console.log("Marker Event: '" + args.eventName +
        "' triggered on: " + args.marker.title +
        ", Lat: " + args.marker.position.latitude + ", Lon: " + args.marker.position.longitude, args);
}

let lastCamera = null;

function onCameraChanged(args) {
    console.log("Camera changed: " + JSON.stringify(args.camera), JSON.stringify(args.camera) === lastCamera);
    lastCamera = JSON.stringify(args.camera);
}

exports.pageLoaded = pageLoaded;
exports.onMapReady = onMapReady;
exports.onCoordinateTapped = onCoordinateTapped;
exports.onMarkerEvent = onMarkerEvent;
exports.onCameraChanged = onCameraChanged;
