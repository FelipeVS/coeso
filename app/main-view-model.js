var observable = require("data/observable");
var HelloWorldModel = (function (_super) {
    __extends(HelloWorldModel, _super);
    function HelloWorldModel() {
        _super.call(this);
        this.set("latitude", -22.92146463919425);
        this.set("longitude", -43.17111968994141);
        this.set("zoom", 11);
        this.set("bearing", 0);
        this.set("tilt", 0);
        this.set("padding", [40, 40, 40, 40]);
        this.set("isLoading", true);
        this.set("isRoutesLoading", false);
    }

    return HelloWorldModel;
})(observable.Observable);
exports.HelloWorldModel = HelloWorldModel;
exports.mainViewModel = new HelloWorldModel();
